import React from 'react';
import Link from 'next/link';
import TelegramLoginButton from 'react-telegram-login';

const links = [
  { href: 'https://www.btobuddy.com', label: 'Home' },
].map(link => {
  link.key = `nav-link-${link.href}-${link.label}`;
  return link;
});

async function handleTelegramResponse(response) {
  let res = await fetch(
    'https://us-central1-portfolio-253005.cloudfunctions.net/function-1?user=' + response.id
  );
  console.log(res);
};

const Nav = () => (
  <nav className="navbar">
    <ul>
      <li>
        <Link href="/">
          <a>Unit Planner</a>
        </Link>
      </li>
      {links.map(({ key, href, label }) => (
        <li key={key}>
          <a href={href}>{label}</a>
        </li>
      ))}
    </ul>

    <style jsx>{`
      :global(body) {
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, Avenir Next, Avenir,
          Helvetica, sans-serif;
      }
      nav {
        text-align: center;
      }
      ul {
        display: flex;
        justify-content: space-between;
      }
      nav > ul {
        padding: 5px 50px;
      }
      li {
        display: flex;
        padding: 6px 8px;
      }
      a {
        color: white;
        text-decoration: none;
        font-size: 20px;
      }
    `}</style>
  </nav>
);

export default Nav;
