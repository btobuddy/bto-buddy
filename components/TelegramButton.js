import React from 'react'

class TelegramButton extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isEnabled: false
    }
  }

  componentDidMount() {
    this.setState({ isEnabled: true })
  }

  render() {
    return (
      <div>
        {this.state.isEnabled ? <div id="telegram-login-btn" /> : "Loading..."}
      </div>
    )
  }
}

export default TelegramButton