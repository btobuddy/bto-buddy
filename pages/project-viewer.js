import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';

import Link from 'next/link';
import ImageMapper from 'react-image-mapper';
import _ from 'lodash';
import { useContext } from 'react';
import UserContext from '../components/UserContext';
import Head from '../components/head';
import Nav from '../components/nav';
import {
  Table,
  Modal,
  Button,
  Switch,
  Form,
  Badge,
  Dropdown,
  Menu,
  Icon,
  Select,
  message
} from 'antd';

const { Option } = Select;

const Home = () => {
  const { userId, state_room, state_bto_date, state_location, state_watchlist } = useContext(UserContext);

  // const [userId, setUserId] = useState('209322679');
  const [userInfo, setUserInfo] = useState({});
  const [units, setUnits] = useState([]);
  const [projects, setProjects] = useState([]);
  const [maps, setMaps] = useState({});
  const [showUnavailableUnits, setShowUnavailableUnits] = useState(false);
  const [currentProject, setCurrentProject] = useState('');
  const [currentMap, setCurrentMap] = useState('');
  const [mappings, setMappings] = useState({
    name: 'units',
    areas: []
  });
  const [watchlist, setWatchlist] = useState([]);
  const [hoveredUnit, setHoveredUnit] = useState('');
  const [expandedStack, setExpandedStack] = useState('');
  const [showModal, setShowModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  

  async function getUnitsV2() {
    setWatchlist(state_watchlist);
    let room_resp = await fetch(
      `/api/units?room=${state_room}&location=${state_location}&bto_date=${state_bto_date}`
    );
    let groupedUnits = await transformData(room_resp.json());

    // Set first map as default first
    let map_resp = await fetch(
      `/api/units_map?location=${state_location}&bto_date=${state_bto_date}`
    );
    let map_urls = await map_resp.json();

    setUnits(groupedUnits);
    console.log(groupedUnits);
    for (var map_url, i = 0; map_url = map_urls[i++];) {
      var project_name = map_url.project;
      var current_url = map_url.url;
      maps[project_name] = current_url;
    }
    // setMaps(map_url);
    setCurrentMap(map_urls[0]['url']);
    setCurrentProject(projects[0]);
    setUserInfo({"bto_date": state_bto_date, "location": state_location,"room": state_room});
  }

  useEffect(async () => {
    setIsLoading(true);
    await getUnitsV2();
    drawMapping();
    setIsLoading(false);
  }, []);

  const transformData = async (newUnits) => {
    let units = await newUnits;
    let transformedUnits = [];
    let groupedProjects = Object.entries(
      _.groupBy(units, unit => unit.project)
    );
    for (let groupedProject of groupedProjects) {
    // Keep track of unique projects
      const [ind_project, units] = groupedProject;
      if (!(projects.includes(ind_project))) {
        projects.push(ind_project);
      }

      let groupedStacks = Object.entries(
        _.groupBy(units, unit => unit.stack)
      );

      for (let groupedStack of groupedStacks) {
        const [stack, units] = groupedStack;
        transformedUnits.push({
          key: stack,
          stack: stack,
          window_direction: units[0].window_direction,
          sun: units[0].sun,
          blocked_view: units[0].blocked_view,
          project: units[0].project,
          coordinates: units[0].coordinates.map(coord => Math.round(coord)),
          units
        });
      }
    }

    return transformedUnits;
  };

  function onSwitchChange(enabled, record) {
    let new_unit = record['unit'];
    if (enabled) {
      watchlist.push(new_unit);
    } else {
      const index = watchlist.indexOf(new_unit);
      watchlist.splice(index, 1);
    }
    // Make API call to update units watchlist
    async function updateWatchlist() {
      let watchlist_str = watchlist.join("add").replace(/#/g,'');
      console.log(watchlist_str)
      let res = await fetch(`https://asia-east2-portfolio-253005.cloudfunctions.net/updateUser?user=${userId}&watch_list=${watchlist_str}`);
    }
    updateWatchlist();
    console.log(watchlist);
  }

  const toggleShowUnavailableUnits = () => {
    setShowUnavailableUnits(!showUnavailableUnits);
  };

  function filterDataUnavailableUnits(unit) {
    if (showUnavailableUnits) {
      return true;
    } else {
      return !unit['is_taken'];
    }
  }

  const renderUnits = () => {
    let columns = [
      {
        title: 'Unit No.',
        dataIndex: 'unit',
        key: 'unit',
        sorter: (a, b) => a.unit.length - b.unit.length,
        render: (text, record) => {
          let status = 'success';
          if (record['is_taken']) {
            status = 'error';
          }
          return (
            <span>
              <Badge status={status} />
              {text}
            </span>
          );
        }
      },
      {
        title: 'Floor',
        dataIndex: 'floor',
        key: 'floor',
        sorter: (a, b) => a.floor - b.floor
      },
      {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
        sorter: (a, b) => a.price.length - b.price.length
      },
      {
        title: 'Watchlist?',
        dataIndex: 'watchlist',
        key: 'watchlist',
        render: (e, record) => (
          <Switch
            onChange={enabled => onSwitchChange(enabled, record)}
            defaultChecked={watchlist.includes(record['unit'])}
            disabled={record['is_taken']}
          />
        )
      }
    ];
    return (
      <div>
        <Form.Item label="Show Unavailable Units">
          <Switch
            checked={showUnavailableUnits}
            onChange={toggleShowUnavailableUnits}
          />
        </Form.Item>
        <Table dataSource={expandedStack.units ? expandedStack.units.filter(filterDataUnavailableUnits) : null} columns={columns}></Table>
      </div>
    );
  };

  const drawMapping = unit => {
    if (unit) {
      setMappings({
        name: 'units',
        areas: [
          {
            name: unit.stack,
            shape: 'circle',
            _id: unit.stack,
            preFillColor: 'rgba(65, 131, 215, 0.5)',
            lineWidth: 'none',
            coords: [...unit.coordinates, 50]
          }
        ]
      });
    } else {
      let areas = Object.values(units).map(unit => {
        return {
          name: unit.stack,
          stack: unit.stack,
          units: unit.units,
          shape: 'circle',
          _id: unit.stack,
          preFillColor: 'rgba(65, 131, 215, 0.5)',
          lineWidth: 'none',
          coords: [...unit.coordinates, 50]
        };
      });
      if (mappings.areas.length !== areas.length) {
        setMappings({
          name: 'units',
          areas
        });
      }
    }
  };

  const LoadMap = mapValues => (
    <span onMouseLeave={() => setMappings({ areas: [] })}>
      <ImageMapper
        src={`${currentMap}?${Math.random()}`}
        width={700}
        imgWidth={3325}
        map={mapValues}
        lineWidth={10}
        active={true}
        onImageMouseMove={() => drawMapping()}
        onMouseEnter={area => setHoveredUnit(area.name)}
        onMouseLeave={() => setHoveredUnit('')}
        onClick={area => {
          setExpandedStack(area);
          toggleModal();
        }}
      />
    </span>
  );

  const columns = [
    {
      title: 'Stack',
      dataIndex: 'stack',
      key: 'stack',
      render: text => <a>{text}</a>,
      sorter: (a, b) => a.stack - b.stack
    },
    {
      title: 'Window Direction',
      dataIndex: 'window_direction',
      key: 'window_direction',
      sorter: (a, b) => a.window_direction.length - b.window_direction.length
    },
    {
      title: 'Sun',
      dataIndex: 'sun',
      key: 'sun',
      sorter: (a, b) => a.sun.length - b.sun.length
    },
    {
      title: 'Blocked View?',
      dataIndex: 'blocked_view',
      key: 'blocked_view',
      sorter: (a, b) => a.blocked_view.length - b.blocked_view.length
    }
  ];

  const toggleModal = () => {
    setShowModal(!showModal);
  };

  function filterDataByProject(unit) {
    return unit['project'] === currentProject;
  }

  async function onSelectProject(project) {
    setCurrentProject(project);
    setCurrentMap(maps[project]);
  }

  return (
    <div>
      <Head title="Home" />
      <Nav />
      <div className="wrapper">
        {/* TODO: Make it load the distinct projects as options */}
        <div className="project-map">
          <Select
            placeholder="Select a Project"
            style={{ width: 300, marginBottom: 20 }}
            onChange={value => onSelectProject(value)}
            value={currentProject}
          >
            {projects.map(project => (
              <Option value={project}>{project}</Option>
            ))}
          </Select>
          {LoadMap(mappings)}
        </div>
        <div className="project-details">
          <Table
            columns={columns}
            dataSource={units.filter(filterDataByProject)}
            loading={isLoading}
            onRow={(area, rowIndex) => {
              return {
                onMouseEnter: () => drawMapping(area),
                onClick: () => {
                  setExpandedStack(area);
                  toggleModal();
                },
                onMouseLeave: () => setMappings({ areas: [] })
              };
            }}
            rowClassName={record =>
              hoveredUnit === record.stack ? 'selected-row' : ''
            }
            pagination={false}
            bordered
            title={() => {
              return (
                <span>
                  <b>BTO Date:</b> {userInfo.bto_date}<br/><b>Location:</b> {userInfo.location}<br/><b>Room:</b> {userInfo.room}
                </span>
              );}
            }
            // expandedRowRender={record => renderUnits(record.units)}
            // expandedRowKeys={[expandedStack]}
          ></Table>
        </div>
        <Modal
          transitionName="slide-right"
          title={`Units under Stack ${expandedStack.stack}`}
          style={{ position: 'absolute', right: 0, top: 0 }}
          bodyStyle={{ height: '100vh', overflow: 'hidden' }}
          visible={showModal}
          onOk={toggleModal}
          onCancel={toggleModal}
          okText="Save"
        >
          <p>
            <b>Window Direction:</b> {expandedStack.window_direction}
          </p>
          <p>
            <b>Sun:</b> {expandedStack.sun}
          </p>
          <p>
            <b>Blocked View:</b> {expandedStack.blocked_view}
          </p>
          {renderUnits()}
        </Modal>
      </div>

      <style jsx>{`
        .wrapper {
          margin: 30px 100px;
          display: flex;
          justify-content: space-around;
        }
        .wrapper > * {
          margin: 30px 0;
        }
        .project-map {
          display: flex;
          flex-direction: column;
        }
        .project-map img {
          width: 100%;
          height: auto;
        }
        .project-details {
          font-size: 12px;
          display: flex;
          flex-direction: column;
        }
        tr {
          height: 50px;
        }
        tbody > tr {
          height: 50px;
        }
      `}</style>
    </div>
  );
};

export default Home;
