import React, { useEffect, useState, useContext } from 'react';
import ReactDOM from 'react-dom';

import Link from 'next/link';
import _ from 'lodash';

import Head from '../components/head';
import UserContext from '../components/UserContext';
import TelegramLoginButton from 'react-telegram-login';


const Landing = () => {
  const { signIn } = useContext(UserContext);

  async function handleTelegramResponse(response) {
    signIn(response.id);
  };

  return (
    <div>
      <Head title="Login" />
      <body>
        <div className="limiter">
          <div className="container-login100">
            <div className="wrap-login100">
              <div className="login100-pic js-tilt" data-tilt>
                <img src="/pineapple.jpg" alt="IMG"></img>
              </div>
              <span className="login100-form-title">
                Member Login
              </span>
              
              <div className="container-login100-form-btn">
                <Link href='/'>
                  <TelegramLoginButton dataOnauth={handleTelegramResponse} botName="btobuddy_bot" buttonSize="large"/>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </body>
      <style jsx>{`
        body {
          background: linear-gradient(-45deg, #ffde59, #ffbd59);
        }
        
        .limiter {
          width: 100%;
          margin: 0 auto;
        }

        .container-login100 {
          width: 100%;  
          min-height: 100vh;
          display: -webkit-box;
          display: -webkit-flex;
          display: -moz-box;
          display: -ms-flexbox;
          display: flex;
          flex-wrap: wrap;
          justify-content: center;
          align-items: center;
          padding: 15px;
          background: #9053c7;
          background: -webkit-linear-gradient(-135deg, #ffde59, #ffbd59);
          background: -o-linear-gradient(-135deg, #ffde59, #ffbd59);
          background: -moz-linear-gradient(-135deg, #ffde59, #ffbd59);
          background: linear-gradient(-135deg, #ffde59, #ffbd59);
        }

        .wrap-login100 {
          width: 960px;
          background: #fff;
          border-radius: 10px;
          overflow: hidden;

          display: -webkit-box;
          display: -webkit-flex;
          display: -moz-box;
          display: -ms-flexbox;
          display: flex;
          flex-wrap: wrap;
          justify-content: center;
          padding: 110px 110px 110px 110px;
          position: relative;
        }

        .login100-pic {
          width: 316px;
          text-align: center;
          
        }

        .login100-pic img {
          max-width: 100%;
          border-radius: 50%;
        }


        .login100-form {
          width: 290px;
        }

        .login100-form-title {
          font-family: Arial, Helvetica, sans-serif;
          font-size: 24px;
          color: #333333;
          line-height: 1.2;
          text-align: center;

          width: 100%;
          display: block;
          padding: 34px;
        }


        .wrap-input100 {
          position: relative;
          width: 100%;
          z-index: 1;
          margin-bottom: 10px;
        }


        [ Button ]*/
        .container-login100-form-btn {
          width: 100%;
          display: -webkit-box;
          display: -webkit-flex;
          display: -moz-box;
          display: -ms-flexbox;
          display: flex;
          flex-wrap: wrap;
          justify-content: center;
          padding-top: 20px;
          align-items: center;
        }

        .login100-form-btn {
          font-family: Montserrat-Bold;
          font-size: 15px;
          line-height: 1.5;
          color: #fff;
          text-transform: uppercase;

          width: 400px;
          height: 50px;
          border-radius: 25px;
          background: #57b846;
          display: -webkit-box;
          display: -webkit-flex;
          display: -moz-box;
          display: -ms-flexbox;
          display: flex;
          justify-content: center;
          align-items: center;
          padding: 0 25px;

          -webkit-transition: all 0.4s;
          -o-transition: all 0.4s;
          -moz-transition: all 0.4s;
          transition: all 0.4s;
        }

        .login100-form-btn:hover {
          background: #333333;
        }


        @media (max-width: 992px) {
          .wrap-login100 {
            padding: 177px 90px 33px 85px;
          }

          .login100-pic {
            width: 35%;
          }

          .login100-form {
            width: 50%;
          }
        }

        @media (max-width: 768px) {
          .wrap-login100 {
            padding: 100px 80px 33px 80px;
          }

          .login100-pic {
            display: none;
          }

          .login100-form {
            width: 100%;
          }
        }

        @media (max-width: 576px) {
          .wrap-login100 {
            padding: 100px 15px 33px 15px;
          }
        }

      `}</style>
    </div>);
};

export default Landing;