// import 'bootstrap/dist/css/bootstrap.min.css';
import 'antd/dist/antd.css';
import './index.css';
import React from 'react';
import App from 'next/app';
import Router from 'next/router';
import UserContext from '../components/UserContext';

export default class MyApp extends App {
  state = {
    user: null
  };

  componentDidMount = () => {
    // Fix remembering of user later
    Router.push('/signin');
    // const userdata = localStorage.getItem('unit-planner-user');
    // if (userdata) {
    //   this.setState(userdata);
    // } else {
    //   Router.push('/signin');
    // }
  };

  signIn = async (username) => {
    let res = await fetch(
      `https://asia-east2-portfolio-253005.cloudfunctions.net/getUser?user=${username}`
    );
    let data = await res.json();
    const userdata = {
      userid: username,
      watchlist: data['watch_list'],
      room: data['room'],
      location: data['location'],
      bto_date: data['bto_date']
    }

    // localStorage.setItem('unit-planner-user', userdata);

    this.setState(userdata
      ,
      () => {
        Router.push('/');
      }
    );
  };

  signOut = () => {
    localStorage.removeItem('unit-planner-user');
    this.setState({
      userdata: null
    });
    Router.push('/signin');
  };

  render() {
    const { Component, pageProps } = this.props;

    return (
      <UserContext.Provider value={{ state_watchlist: this.state.watchlist, userId: this.state.userid,
        state_room: this.state.room, state_location: this.state.location,
        state_bto_date: this.state.bto_date, signIn: this.signIn, signOut: this.signOut }}>
        <Component {...pageProps} />
      </UserContext.Provider>
    );
  }
}
// This default export is required in a new `pages/_app.js` file.
// export default function MyApp({ Component, pageProps }) {
//   return <Component {...pageProps} />;
// }
