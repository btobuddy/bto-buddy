const db = require('../services/db');
const Types = db.Sequelize.DataTypes;

const siteplan_images = db.sequelize.define('siteplan_images', {
  project: Types.STRING,
  bto_date: Types.STRING,
  location: Types.STRING,
  url: Types.STRING
});

module.exports = siteplan_images;
