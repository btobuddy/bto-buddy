const db = require('../services/db');
const Types = db.Sequelize.DataTypes;

const siteplan = db.sequelize.define(
  'siteplan',
  {
    bto_date: Types.STRING,
    location: Types.STRING,
    room: Types.STRING,
    unit: Types.STRING,
    floor: Types.STRING,
    stack: Types.STRING,
    window_direction: Types.STRING,
    sun: Types.STRING,
    blocked_view: Types.STRING,
    price: Types.STRING,
    coordinates: Types.STRING,
    is_taken: Types.BOOLEAN
  },
  { freezeTableName: true }
);

module.exports = siteplan;
